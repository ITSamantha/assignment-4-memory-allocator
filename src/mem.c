#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ...);

void debug(const char* fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header* block) {
    return block && block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void const* restrict addr, block_size block_sz, void* restrict next) {
    *((struct block_header*)addr) = (struct block_header){
        .next = next,
        .capacity = capacity_from_size(block_sz),
        .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region* r);

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap((void*)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const* addr, size_t query) {
    size_t block_length = region_actual_size(query + offsetof(struct block_header, contents));
    block_size size = (block_size){ .bytes = block_length };
    void* block_addr = map_pages(addr, block_length, MAP_FIXED_NOREPLACE);
    if (block_addr == MAP_FAILED) {
        block_addr = map_pages(addr, block_length, 0);
        if (block_addr == MAP_FAILED) {
            return REGION_INVALID;
        }
    }
    block_init(block_addr, size, NULL);
    struct region region = (struct region){
        .addr = block_addr,
        .size = block_length,
        .extends = addr == block_addr
    };
    return region;
}

static void* block_after(struct block_header const* block);

void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой) --- */

static bool block_splittable(struct block_header* restrict block, size_t query) {
    return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    if (block && block_splittable(block, query)) {
        struct block_header* block_next = block->next;
        block_capacity old_block_capacity = block->capacity;
        block->capacity = (block_capacity){ .bytes = query };
        block_init(
            block->contents + block->capacity.bytes,
            (block_size) {
            .bytes = old_block_capacity.bytes - block->capacity.bytes
        },
            block_next
                );
        block->next = (struct block_header*)(block->contents + block->capacity.bytes);
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after(struct block_header const* block) {
    return (void*)(block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
    struct block_header const* fst,
    struct block_header const* snd) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header* block) {
    if (block && block->next && mergeable(block, block->next)) {
        block->capacity = (block_capacity){ .bytes = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes };
        block->next = block->next->next;
        return true;
    }
    return false;
}


/*  --- ... если размера кучи хватает --- */

enum block_search_result_type {
    BSR_FOUND_GOOD_BLOCK,
    BSR_REACHED_END_NOT_FOUND,
    BSR_CORRUPTED
};

struct block_search_result {
    enum block_search_result_type type;
    struct block_header* block;
};

static struct block_search_result make_block_search_result(enum block_search_result_type type, struct block_header* block) {
    return (struct block_search_result) {
        .type = type,
            .block = block
    };
}

static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
    if (!block) {
        return make_block_search_result(BSR_CORRUPTED, NULL);
    }
    struct block_header* current_block = block;
    while (current_block) {
        while (try_merge_with_next(current_block));
        if (current_block->is_free && block_is_big_enough(sz, current_block)) {
            return make_block_search_result(BSR_FOUND_GOOD_BLOCK, current_block);
        }
        if (!current_block->next) break;
        current_block = current_block->next;
    }
    return make_block_search_result(BSR_REACHED_END_NOT_FOUND, current_block);
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    if (block) {
        query = size_max(query, BLOCK_MIN_CAPACITY);
        struct block_search_result requested_block = find_good_or_last(block, query);
        if (requested_block.type == BSR_FOUND_GOOD_BLOCK) {
            split_if_too_big(requested_block.block, query);
            requested_block.block->is_free = false;
        }
        return requested_block;
    }
    return make_block_search_result(BSR_CORRUPTED, NULL);
}

static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    struct block_header* actual_addr = NULL;
    if (last) {
        void* next_region_first_block = block_after(last);
        struct region allocated_region = alloc_region(next_region_first_block, query);
        if (!region_is_invalid(&allocated_region)) {
            actual_addr = allocated_region.addr;
            last->next = actual_addr;
            if (try_merge_with_next(last)) {
                actual_addr = last;
            }
        }
    }
    return actual_addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    if (heap_start) {
        struct block_search_result memalloc_result = try_memalloc_existing(query, heap_start);
        switch (memalloc_result.type) {
        case BSR_REACHED_END_NOT_FOUND: {
            struct block_header* new_region_first_block = grow_heap(memalloc_result.block, query);
            memalloc_result = try_memalloc_existing(query, new_region_first_block);
            if (memalloc_result.type == BSR_FOUND_GOOD_BLOCK) {
                return memalloc_result.block;
            }
            else {
                return NULL;
            }
        }
        case BSR_FOUND_GOOD_BLOCK: {
            return memalloc_result.block;
        }
        default: {
            return NULL;
        }
        }
    }
    return NULL;
}

void* _malloc(size_t query) {
    struct block_header* const addr = memalloc(query, (struct block_header*)HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*)(((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void _free(void* mem) {
    if (!mem) return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}