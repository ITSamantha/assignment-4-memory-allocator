#include "test.h"

void run_all_tests() {
    test_allocate_memory_once();
    test_free_memory_once();
    test_free_memory_twice();
    test_allocate_when_region_filled();
    test_allocate_when_next_region_filled();
}

int main() {

    run_all_tests();
    return 0;

}
