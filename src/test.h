#pragma once
#include "mem.h"
#include "mem_internals.h"
#include <stdint.h>
#include <stdio.h>

#define HEAP_SIZE 13000

static struct block_header *block_get_header(void *contents);
static void remove_heap(void* heap);
void test_allocate_memory_once();
void test_free_memory_once();
void test_free_memory_twice();
void test_allocate_when_region_filled();
void test_allocate_when_next_region_filled();