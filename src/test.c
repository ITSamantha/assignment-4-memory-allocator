#include "test.h"

#define HEAP_SIZE 13000

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}
static void remove_heap(void* heap) {
    munmap(heap, size_from_capacity((block_capacity) { .bytes = HEAP_SIZE }).bytes);
}
static void print_error(char* error) {
    fprintf(stderr, "Test failed: %s \n", error);
}
static void print(char* message) {
    fprintf(stdout, "%s \n", message);
}

void test_allocate_memory_once() {
    print("Test 1");
    void* heap = heap_init(106010);
    if (heap == NULL) {
        print_error("Unable to init heap");
        return;
    }
    int32_t* common_memory = _malloc(4000);
    struct block_header* header = block_get_header(common_memory);
    if (common_memory == NULL || header->is_free) {
        if (common_memory == NULL) {
            print_error("Unable to allocate memory");
        } else if (header->is_free) {
            print_error("Allocated memory is free");
        }
        remove_heap(heap);
        return;
    }
    debug_heap(stdout, heap);
    print("Test 1 passed");
    remove_heap(heap);
}

void test_free_memory_once() {
    print("Test 2");
    void* heap = heap_init(2 * 4096);
    if (heap == NULL) {
        print_error("Unable to init heap");
        return;
    }
    int32_t* common_memory1 = _malloc(4000);
    int32_t* common_memory2 = _malloc(4000);
    _free(common_memory1);

    struct block_header* header1 = block_get_header(common_memory1);
    struct block_header* header2 = block_get_header(common_memory2);
    if (common_memory1 == NULL || common_memory2 == NULL || !header1->is_free || header2->is_free) {
        if (common_memory1 == NULL) {
            print_error("Unable to allocate memory (first block)");
        } else if (common_memory2 == NULL) {
            print_error("Unable to allocate memory (second block)");
        } else if (!header1->is_free) {
            print_error("Allocated memory (first block) is not free");
        } else if (header2->is_free) {
            print_error("Allocated memory (second block) is free");
        }
        remove_heap(heap);
        return;
    }
    debug_heap(stdout, heap);
    print("Test 2 passed");
    remove_heap(heap);
}

void test_free_memory_twice() {
    print("Test 3");
    void* heap = heap_init(2 * 4096);
    if (heap == NULL) {
        print_error("Unable to init heap");
        return;
    }
    int32_t* common_memory1 = _malloc(4000);
    int32_t* common_memory2 = _malloc(4000);
    _free(common_memory1);
    _free(common_memory2);

    struct block_header* header1 = block_get_header(common_memory1);
    struct block_header* header2 = block_get_header(common_memory2);
    if (common_memory1 == NULL || common_memory2 == NULL || !header1->is_free || !header2->is_free) {
        if (common_memory1 == NULL) {
            print_error("Unable to allocate memory (first block)");
        } else if (common_memory2 == NULL) {
            print_error("Unable to allocate memory (second block)");
        } else if (!header1->is_free) {
            print_error("Allocated memory (first block) is not free");
        } else if (header2->is_free) {
            print_error("Allocated memory (second block) is not free");
        }
        remove_heap(heap);
        return;
    }
    debug_heap(stdout, heap);
    print("Test 3 passed");
    remove_heap(heap);
}

void test_allocate_when_region_filled() {
    print("Test 4");
    void* heap = heap_init(2 * 4096);
    if (heap == NULL) {
        print_error("Unable to init heap");
        return;
    }
    int32_t* common_memory1 = _malloc(8000);
    debug_heap(stdout, heap);
    int32_t* common_memory2 = _malloc(8000);
    struct block_header* header1 = block_get_header(common_memory1);
    struct block_header* header2 = block_get_header(common_memory2);
    if (common_memory1 == NULL || common_memory2 == NULL || header1->is_free || header2->is_free) {
        if (common_memory1 == NULL) {
            print_error("Unable to allocate memory (first block)");
        } else if (common_memory2 == NULL) {
            print_error("Unable to allocate memory (second block)");
        } else if (!header1->is_free) {
            print_error("Allocated memory (first block) is free");
        } else if (header2->is_free) {
            print_error("Allocated memory (second block) is free");
        }
        remove_heap(heap);
        return;
    }
    debug_heap(stdout, heap);
    print("Test 4 passed");
    remove_heap(heap);
}

void test_allocate_when_next_region_filled() {
    print("Test 5");
    void* heap = heap_init(2 * 4096);
    if (heap == NULL) {
        print_error("Unable to init heap");
        return;
    }
    int32_t* common_memory1 = _malloc(8000);
    debug_heap(stdout, heap);
    struct block_header* header1 = block_get_header(common_memory1);
    void* address_after_region = header1->contents + header1->capacity.bytes;
    void *test_mem = mmap(address_after_region, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    if (test_mem == NULL) {
        print_error("Unable to allocate memory between regions");
        remove_heap(heap);
        return;
    }
    int32_t* common_memory2 = _malloc(8000);
    struct block_header* header2 = block_get_header(common_memory2);
    if (common_memory1 == NULL || common_memory2 == NULL || header1->is_free || header2->is_free) {
        if (common_memory1 == NULL) {
            print_error("Unable to allocate memory (first block)");
        } else if (common_memory2 == NULL) {
            print_error("Unable to allocate memory (second block)");
        } else if (!header1->is_free) {
            print_error("Allocated memory (first block) is free");
        } else if (header2->is_free) {
            print_error("Allocated memory (second block) is free");
        }
        remove_heap(heap);
        return;
    }
    debug_heap(stdout, heap);
    print("Test 5 passed");
    remove_heap(heap);
}